<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>INSKAN</title>

	<?php include '../links.php'; ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

    	<?php include '../header.php'; ?>

    	<div id="wrapper">

    		<!-- Navigation -->
    		
    		<div id="page-wrapper">
    			<div class="row">
    				<div class="col-lg-12">
    					<h1 class="page-header">Dashboard</h1>
    				</div>
    				<!-- /.col-lg-12 -->
    			</div>
    			<!-- /.row -->
    			<h3>EDIT DATA BARANG</h3>
    			
    			<?php
    			$id = $_GET['id_jenis'];
    			$data = mysqli_query($koneksi,"select * from jenis where id_jenis='$id'");
    			while($d = mysqli_fetch_array($data)){
    				?>
    				<div class="panel-body">
    					<div class="row">
    						<div class="col-lg-12">
    							<form method="post" action="update_jenis.php">
    								<table>
    									<tr>			
    										<td>NAMA</td>
    										<td>
    											<input type="hidden" name="id_jenis" value="<?php echo $d['id_jenis']; ?>">
    											<input type="text" name="nama_jenis" value="<?php echo $d['nama_jenis']; ?>">
    										</td>
    									</tr>
    									<tr>
    										<td>KODE JENIS</td>
    										<td><input type="text" name="kode_jenis" value="<?php echo $d['kode_jenis']; ?>"></td>
    									</tr>
    									<tr>
    										<td>KETERANGAN</td>
    										<td><input type="text" name="keterangan" value="<?php echo $d['keterangan']; ?>"></td>
    									</tr>
    						<tr>
    							<td></td>
    							<td><input type="submit" value="SIMPAN"></td>
    						</tr>		
    					</table>
    				</form>
    				<?php 
    			}
    			?>
    			
    			<!-- /.row -->
    			<!-- /.row -->
    		</div>
    		<!-- /#page-wrapper -->
    	</div>
    </div>
</div>
</div>
<!-- /#wrapper -->

<!-- jQuery -->

<?php include '../scripts.php'; ?>
</body>

</html>

