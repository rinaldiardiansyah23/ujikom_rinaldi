<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>INSKAN</title>
    <?php include '../links.php'; ?>
</head>
<body>
    <?php include '../header.php'; ?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">JENIS</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <a href="./tambah_jenis.php" class="btn btn-success" type="button" name="submit">Tambah</a>
                        <a href="./cetak_jenis.php" class="btn btn-primary" type="button" name="submit">Cetak</a>
                        <a href="./export_excel_jenis.php" class="btn btn-danger" type="button" name="submit">Export</a>
                            <div class="dataTable_wrapper"><br>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <td>No</td>
                                            <td>Nama jenis</td>
                                            <td>Kode jenis</td>
                                            <td>Keterangan</td>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no=1;
                                        $data=mysqli_query($koneksi,"SELECT * FROM jenis"); 
                                        while ($tampil=mysqli_fetch_array($data)){
                                        echo "<tr>";
                                            echo "<td>$no</td>";
                                            echo "<td>$tampil[nama_jenis]</td>";
                                            echo "<td>$tampil[kode_jenis]</td>";
                                            echo "<td>$tampil[keterangan]</td>";
                                            echo "<td>
                                                <a href='edit_jenis.php?id_jenis=$tampil[id_jenis]'>Edit</a>
                                                <a href='hapus_jenis.php?id_jenis=$tampil[id_jenis]'>Hapus</a>
                                                </td>";
                                        echo "</tr>";
                                        
                                        $no++;}
                                    ?>  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php include '../scripts.php'; ?>
</body>
</html>