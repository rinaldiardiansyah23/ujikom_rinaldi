<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>INSKAN - Barang</title>
    <?php include '../links.php'; ?>
</head>
<body>
    <?php include '../header.php'; ?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">BARANG</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <a href="./tambah_barang.php" class="btn btn-success" type="button" name="submit">Tambah</a>
                        <a href="./cetak_barang.php" class="btn btn-danger" type="button" name="submit">Cetak</a>
                        <a href="./export_excel_barang.php" class="btn btn-primary" type="button" name="submit">Export</a>
                            <div class="dataTable_wrapper"><br>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <td>No</td>
                                            <td>Nama</td>
                                            <td>Kondisi</td>
                                            <td>Keterangan</td>
                                            <td>Jumlah</td>
                                            <td>jenis</td>
                                            <td>Tanggal</td>
                                            <td>Nama ruang</td>
                                            <td>Kode</td>
                                            <td>Petugas</td>    
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no=1;
                                        $data=mysqli_query($koneksi,"SELECT i.*,r.nama_ruang,j.nama_jenis,p.nama_petugas FROM inventaris i LEFT join ruang r ON i.id_ruang=r.id_ruang LEFT JOIN jenis j ON i.id_jenis=j.id_jenis LEFT JOIN petugas p ON i.id_petugas=p.id_petugas"); 
                                        while ($tampil=mysqli_fetch_array($data)){
                                        echo "<tr>";
                                            echo "<td>$no</td>";
                                            echo "<td>$tampil[nama]</td>";
                                            echo "<td>$tampil[kondisi]</td>";
                                            echo "<td>$tampil[keterangan]</td>";
                                            echo "<td>$tampil[jumlah]</td>";
                                            echo "<td>$tampil[nama_jenis]</td>";
                                            echo "<td>$tampil[tanggal_register]</td>";
                                            echo "<td>$tampil[nama_ruang]</td>";
                                            echo "<td>$tampil[kode_inventaris]</td>";
                                            echo "<td>$tampil[nama_petugas]</td>";
                                            echo "<td>
                                                <a href='edit_barang.php?id=$tampil[id_inventaris]'>Edit</a>
                                                <a href='hapus_barang.php?id_inventaris=$tampil[id_inventaris]''>Hapus</a>
                                                </td>";
                                        echo "</tr>";
                                        
                                        $no++;}
                                    ?>  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php include 'scripts.php'; ?>
</body>
</html>
