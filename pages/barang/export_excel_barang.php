<?php
include "../../config/koneksi.php";

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data-barang.xls");
 
// Tambahkan table
?>

<p align="center" id="title-laporan">DATA BARANG</p><br>
<a type="button" value="EXCEL" href="" id="cetak" class="no-print"> </a>

<table border="1" width="100%" style="border-collapse: collapse;">
	<thead class="title-table">
		<tr style="height: 40px;">
			<th>No</th>
			<th>Nama Barang</th>
			<th>Kondisi</th>
			<th>Keterangan</th>
			<th>Jumlah</th>
			<th>Nama Jenis</th>
			<th>Tanggal Register</th>
			<th>Nama ruang</th>
			<th>Kode inventaris</th>
			<th>Nama_petugas</th>
		</tr>
	</thead>
	<tbody>
		 <?php
			$no=1;
			$data=mysqli_query($koneksi,"SELECT i.*,r.nama_ruang,j.nama_jenis,p.nama_petugas FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang JOIN jenis j ON i.id_jenis=j.id_jenis JOIN petugas p ON i.id_petugas=p.id_petugas");
			while($ba=mysqli_fetch_array($data)) {
				echo"<tr>
						<td class='text-center'>$no</td>
						<td class='text-center'>$ba[nama]</td>
						<td class='text-center'>$ba[kondisi]</td>
						<td class='text-center'>$ba[keterangan]</td>
						<td class='text-center'>$ba[jumlah]</td>
						<td class='text-center'>$ba[nama_jenis]</td>
						<td class='text-center'>$ba[tanggal_register]</td>
						<td class='text-center'>$ba[nama_ruang]</td>
						<td class='text-center'>$ba[kode_inventaris]</td>
						<td class='text-center'>$ba[nama_petugas]</td>
					</tr>";$no++;
			}
		?>
	</tbody>
</table>



