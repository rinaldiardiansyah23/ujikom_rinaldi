<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>INSKAN</title>
    <?php include '../links.php'; ?>
</head>
<body>
    <?php include '../header.php'; ?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">PENGEMBALIAN</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal Pinjam</th>
                                            <th>Nama Peminjam</th>
                                            <th>Nama Barang</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;
                                        $query = mysqli_query($koneksi,"SELECT * FROM peminjam p JOIN petugas pg ON p.id_petugas=pg.id_petugas WHERE p.status='Sedang dipinjam' ");
                                        while ($data = mysqli_fetch_array($query)){
                                            $query1=mysqli_query($koneksi,"SELECT d.*,i.nama FROM detail_pinjam d JOIN inventaris i ON d.id_inventaris=i.id_inventaris WHERE d.id_peminjaman='$data[id_peminjaman]'");
                                            $data1=mysqli_fetch_assoc($query1);
                                        ?>
                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $data['tgl_pinjam']; ?></td>
                                            <td><?php echo $data['nama_petugas']; ?></td>
                                            <td><?php echo $data1['nama']; ?></td>
                                            <td>
                                                <a href="proses_kembali.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>&id-inventaris=<?=$data1['id_inventaris'];?>&jumlah=<?=$data1['jumlah'];?>" class="btn btn-primary">Kembalikan</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <?php
                                }?>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php include '../scripts.php'; ?>
</body>
</html>
