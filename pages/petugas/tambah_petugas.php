<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>INSKAN</title>

    <?php include '../links.php'; ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php include '../header.php'; ?>
    <div id="wrapper">
 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                 <form action="simpan_petugas.php" method="post">
                                     
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input name="username" class="form-control" placeholder="Masukan Username Anda">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input name="password" class="form-control" type="password" placeholder="Masukan Password">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Petugas</label>
                                            <input name="nama_petugas" class="form-control" type="text" placeholder="Masukan Password">
                                        </div>
                                        <div class="form-group">
                                            <label>Level</label>
                                        <select name="id_level" class="form-control">
                                        <option hidden>Pilih Level</option>
                                        <?php
                                            $query=mysqli_query($koneksi,"SELECT * FROM level");
                                            while ($show=mysqli_fetch_array($query)) {
                                            echo "<option value='$show[id_level]'>$show[nama_level]
                                            </option>";
                                        }
                                        ?>
                                        </select>
                                        </div>
                                        <button type="submit" class="btn btn-default">Simpan</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
</div>
    <?php include '../scripts.php'; ?>
</body>
</html>