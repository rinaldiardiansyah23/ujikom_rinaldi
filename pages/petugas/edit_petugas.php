<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>SB Admin 2 - Bootstrap Admin Theme</title>

	<?php include '../links.php'; ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

    	<?php include '../header.php'; ?>

    	<div id="wrapper">

    		<!-- Navigation -->
    		
    		<div id="page-wrapper">
    			<div class="row">
    				<div class="col-lg-12">
    					<h1 class="page-header">Dashboard</h1>
    				</div>
    				<!-- /.col-lg-12 -->
    			</div>
    			<!-- /.row -->
    			<h3>EDIT DATA BARANG</h3>
    			
    			<?php
    			$id = $_GET['id_petugas'];
    			$data = mysqli_query($koneksi,"select * from petugas where id_petugas='$id'");
    			while($d = mysqli_fetch_array($data)){
    				?>
    				<div class="panel-body">
    					<div class="row">
    						<div class="col-lg-12">
    							<form method="post" action="update_petugas.php">
    								<table>
    									<tr>			
    										<td>Username</td>
    										<td>
    											<input type="hidden" name="id_petugas" value="<?php echo $d['id_petugas']; ?>">
    											<input class="form-control" type="text" name="username" value="<?php echo $d['username']; ?>">
    										</td>
    									</tr>
    									<tr>
    										<td>Password</td>
    										<td><input class="form-control" type="password" name="password" value="<?php echo $d['password']; ?>"></td>
    									</tr>
    									<tr>
                                            <td>Nama Petugas</td>
                                            <td><input class="form-control" type="text" name="nama_petugas" value="<?php echo $d['nama_petugas']; ?>"></td>
                                        </tr>
                                        <tr>
    										<td>level</td>
    										<td>
                                            <select name="id_level" class="form-control">
                                        <option hidden>Pilih Level</option>
                                        <?php
                                            $query=mysqli_query($koneksi,"SELECT * FROM level");
                                            while ($show=mysqli_fetch_array($query)) {
                                                if($d['id_level'] == $show['id_level']){
                                                    echo "<option value='$show[id_level]' selected>$show[nama_level] </option>"; 
                                                }else{
                                                    echo "<option value='$show[id_level]'>$show[nama_level] </option>"; 
                                                }
                                            }
                                        ?>
                                        </select>
                                    </td>
    									</tr>
    						<tr>
    							<td></td>
    							<td><input type="submit" value="SIMPAN"></td>
    						</tr>		
    					</table>
    				</form>
    				<?php 
    			}
    			?>
    			
    			<!-- /.row -->
    			<!-- /.row -->
    		</div>
    		<!-- /#page-wrapper -->
    	</div>
    </div>
</div>
</div>
<!-- /#wrapper -->

<!-- jQuery -->

<?php include '../scripts.php'; ?>
</body>

</html>

