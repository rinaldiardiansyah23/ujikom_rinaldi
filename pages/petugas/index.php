<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>INSKAN</title>
    <?php include '../links.php'; ?>
</head>
<body>
    <?php include '../header.php'; ?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Petugas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                        <a href="./tambah_petugas.php" class="btn btn-success" type="button" name="submit">Tambah</a>
                        <a href="./cetak_petugas.php" class="btn btn-primary" type="button" name="submit">Cetak</a>
                        <a href="./export_excel_petugas.php" class="btn btn-danger" type="button" name="submit">Export</a>
                            <div class="dataTable_wrapper"><br>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <td>No</td>
                                            <td>Username</td>
                                            <td>Password</td>
                                            <td>Nama Petugas</td>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no=1;
                                        $data=mysqli_query($koneksi,"SELECT * FROM petugas"); 
                                        while ($tampil=mysqli_fetch_array($data)){
                                        echo "<tr>";
                                            echo "<td>$no</td>";
                                            echo "<td>$tampil[username]</td>";
                                            echo "<td>$tampil[password]</td>";
                                            echo "<td>$tampil[nama_petugas]</td>";
                                            echo "<td>
                                                <a href='edit_petugas.php?id_petugas=$tampil[id_petugas]'>Edit</a>
                                                <a href='hapus_petugas.php?id_petugas=$tampil[id_petugas]''>Hapus</a>
                                                </td>";
                                        echo "</tr>";
                                        
                                        $no++;}
                                    ?>  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php include '../scripts.php'; ?>
</body>
</html>
