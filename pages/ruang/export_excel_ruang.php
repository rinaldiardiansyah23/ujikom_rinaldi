<?php
include "koneksi.php";

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data-ruang.xls");
 
// Tambahkan table
?>

<p align="center" id="title-laporan">DATA RUANG</p><br>
<a type="button" value="EXCEL" href="" id="cetak" class="no-print"> </a>

<table border="1" width="100%" style="border-collapse: collapse;">
	<thead class="title-table">
		<tr style="height: 40px;">
			<th>No</th>
			<td>Nama ruang</td>
            <td>Kode ruang</td>
            <td>Keterangan</td>
		</tr>
	</thead>
	<tbody>
		 <?php
			$no=1;
			$data=mysqli_query($koneksi,"SELECT * FROM ruang");
			while($ba=mysqli_fetch_array($data)) {
				echo"<tr>
						<td class='text-center'>$no</td>
						<td class='text-center'>$ba[nama_ruang]</td>
						<td class='text-center'>$ba[kode_ruang]</td>
						<td class='text-center'>$ba[keterangan]</td>
					</tr>";$no++;
			}
		?>
	</tbody>
</table>



