<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <title>INSKAN</title>
        <?php include '../links.php'; ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php include '../header.php'; ?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form peminjaman</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <form action="proses_pinjam.php" method="POST">
                                <input type="hidden" name="id_inventaris" value="<?=$show['id_inventaris'];?>">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Nama Peminjam:</label>
                                    <input type="text" class="form-control" value="<?=$_SESSION['nama_petugas'];?>" name="nama_peminjam" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama Barang:</label>
                                    <select name="id_inventaris" class="form-control">
                                            <option hidden>Pilih jenis</option>
                                            <?php
                                                $query=mysqli_query($koneksi,"SELECT * FROM inventaris");
                                                while ($show=mysqli_fetch_array($query)) {
                                                    echo "<option value='$show[id_inventaris]'>$show[nama]</option>";
                                                }
                                            ?>
                                            </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Jumlah</label>
                                    <input type="text" class="form-control"  name="jumlah">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Tanggal Peminjam:</label>
                                    <input type="text" class="form-control"  name="tgl_pinjam" value="<?php echo date("Y-m-d"); ?>" disabled>
                                </div>
                            <button type="submit" class="btn btn-default">Simpan</button>
                        </form>
                        </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php
    include"../scripts.php";
?>

</body>

</html>