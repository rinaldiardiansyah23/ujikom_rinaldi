<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;background: #3cc5ea;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?=$base_url;?>pages/"><b>INS</b>KAN</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <!-- /.dropdown -->
        <!-- /.dropdown -->
        
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                
                <li><a href="<?=$base_url;?>pages/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="<?=$base_url;?>pages/index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>

                <?php if ($_SESSION['akses'] == 'Administrator'){?>
                
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Master data<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=$base_url;?>pages/barang/">Barang</a>
                        </li>
                        <li>
                            <a href="<?=$base_url;?>pages/jenis/">Jenis</a>
                        </li>
                        <li>
                            <a href="<?=$base_url;?>pages/ruang/">Ruang</a>
                        </li>
                        <li>
                            <a href="<?=$base_url;?>pages/petugas/">Petugas</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
               <li>
                    <a href="<?=$base_url;?>pages/peminjaman/"><i class="fa fa-dashboard fa-fw"></i> Peminjaman</a>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="<?=$base_url;?>pages/pengembalian/"><i class="fa fa-reply fa-fw"></i> Pengembalian</a>
                </li>
                <li>
                    <a href="<?=$base_url;?>pages/laporan/"><i class="fa fa-dashboard fa-fw"></i> Laporan</a>
                </li>
                <?php } ?>

                <?php if ($_SESSION['akses'] == 'Operator'){?>
                
               <li>
                    <a href="<?=$base_url;?>pages/peminjaman/"><i class="fa fa-dashboard fa-fw"></i> Peminjaman</a>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="<?=$base_url;?>pages/pengembalian/"><i class="fa fa-reply fa-fw"></i> Pengembalian</a>
                </li>
                <?php } ?>

                <?php if ($_SESSION['akses'] == 'Peminjam'){?>
                
               <li>
                    <a href="<?=$base_url;?>pages/peminjaman"><i class="fa fa-dashboard fa-fw"></i> Peminjaman</a>
                    <!-- /.nav-second-level -->
                </li>
                <?php } ?>
                </ul>
                    </ul>
                    <!-- /.nav-second-level -->
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</li>
</nav>
