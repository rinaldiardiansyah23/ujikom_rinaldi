<?php
include"../../config/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>INSKAN</title>
    <?php include '../links.php'; ?>
</head>
<body>
    <?php include '../header.php'; ?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">LAPORAN</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <a href="./cetak_laporan.php" class="btn btn-primary" type="button" name="submit">Cetak</a>
                        <a href="./export_excel_laporan.php" class="btn btn-danger" type="button" name="submit">Export</a>
                            <div class="dataTable_wrapper"><br>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal Pinjam</th>
                                            <th>Tanggal Pengembalian</th>
                                            <th>Nama Peminjam</th>
                                            <th>Nama Barang</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;
                                        $query = mysqli_query($koneksi,"SELECT * FROM peminjam p JOIN petugas pg ON p.id_petugas=pg.id_petugas WHERE p.status='Sudah Dikembalikan' ");
                                        while ($data = mysqli_fetch_array($query)){
                                            $query1=mysqli_query($koneksi,"SELECT d.*,i.nama FROM detail_pinjam d JOIN inventaris i ON d.id_inventaris=i.id_inventaris WHERE d.id_peminjaman='$data[id_peminjaman]'");
                                            $data1=mysqli_fetch_assoc($query1);
                                        ?>
                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $data['tgl_pinjam']; ?></td>
                                            <td><?php echo $data['tgl_kembalikan']; ?></td>
                                            <td><?php echo $data['nama_petugas']; ?></td>
                                            <td><?php echo $data1['nama']; ?></td>
                                            <td><?php echo $data['status'] ?></td>
                                        </tr>
                                    </tbody>
                                    <?php
                                }?>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php include '../scripts.php'; ?>
</body>
</html>
