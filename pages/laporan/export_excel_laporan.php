<?php
include"../../config/koneksi.php";
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data-laporan.xls");
 
// Tambahkan table
?>

<p align="center" id="title-laporan">DATA LAPORAN</p><br>
<p>Laporan pada tanggal : <?php echo date("Y/m/d"); ?></p>
<table border="1" width="100%" style="border-collapse: collapse;">
	<thead class="title-table">
		<tr style="height: 40px;">
            <th>No</th>
            <th>Tanggal Pinjam</th>
            <th>Tanggal Pengembalian</th>
            <th>Nama Peminjam</th>
            <th>Nama Barang</th>
            <th>Status</th>
        </tr>
	</thead>
	<tbody>
		 <?php
			$no = 1;
		    $query = mysqli_query($koneksi,"SELECT * FROM peminjam p JOIN petugas pg ON p.id_petugas=pg.id_petugas WHERE p.status='Sudah Dikembalikan' ");
		    while ($data = mysqli_fetch_array($query)){
		        $query1=mysqli_query($koneksi,"SELECT d.*,i.nama FROM detail_pinjam d JOIN inventaris i ON d.id_inventaris=i.id_inventaris WHERE d.id_peminjaman='$data[id_peminjaman]'");
		        $data1=mysqli_fetch_assoc($query1);
		    ?>
		    <tr>
		        <td><?php echo $no++ ?></td>
		        <td><?php echo $data['tgl_pinjam']; ?></td>
		        <td><?php echo $data['tgl_kembalikan']; ?></td>
		        <td><?php echo $data['nama_petugas']; ?></td>
		        <td><?php echo $data1['nama']; ?></td>
		        <td><?php echo $data['status'] ?></td>
		    </tr>
		</tbody>
		<?php
		}?>
	</tbody>
</table>



