<?php
    include "../../config/koneksi.php";
    if(isset($_SESSION['nama_petugas'])){
        echo"<script>window.location='../index.php'</script>";
    }else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Rinaldi Ardiansyah - UJIKOM">
    <title>INSKAN - Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
    <style type="text/css">
        body{
            /*background: url(./gambar/19156723346_89b19442e1.jpg) no-repeat;*/
            width: 100%;
            background-position: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="./p_login.php" method="POST">
                            <div class="form-group">
                                <input class="form-control" placeholder="Username" name="username" type="text" maxlength="100" autofocus required/>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="" required/>
                            </div>
                            <!-- <a href="#">Forgot Password?</a> -->
                            <!-- Change this to a button or input when using this as a form -->
                            <button class="btn btn-lg btn-success btn-block type="
                            submit" ">Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php
}
?>