<?php
    include '../config/koneksi.php';
    if(empty($_SESSION['nama_petugas'])){
        echo"<script>window.alert('Anda Belum Login, Silahkan Login Terlebih Dahulu !');window.location='./login/index.php'</script>";
    }else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>INSKAN</title>
    <?php include 'links.php'; ?>
</head>
<body>
<?php include 'header.php'; ?>
    <div id="wrapper">
        <!-- Navigation -->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-laptop fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php
                                        $query=mysqli_query($koneksi,"SELECT sum(jumlah) as jumlah FROM inventaris WHERE id_jenis='1'");
                                        $hasil=mysqli_fetch_assoc($query);
                                        echo $hasil['jumlah'];
                                    ?>
                                    </div>
                                    <div>Laptop</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php
                                        $query=mysqli_query($koneksi,"SELECT sum(jumlah) as jumlah FROM inventaris WHERE id_jenis='5'");
                                        $hasil=mysqli_fetch_assoc($query);
                                        echo $hasil['jumlah'];
                                    ?>
                                    </div>
                                    <div>Proyektor</div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php
                                        $query=mysqli_query($koneksi,"SELECT sum(jumlah) as jumlah FROM inventaris WHERE id_jenis='3'");
                                        $hasil=mysqli_fetch_assoc($query);
                                        echo $hasil['jumlah'];
                                    ?>
                                    </div>
                                    <div>Buku</div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-book fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php
                                        $query=mysqli_query($koneksi,"SELECT sum(jumlah) as jumlah FROM inventaris WHERE id_jenis='4'");
                                        $hasil=mysqli_fetch_assoc($query);
                                        echo $hasil['jumlah'];
                                    ?>
                                    </div>
                                    <div>Alat</div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <!-- /.row -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php
}
?>